﻿using System.Diagnostics;

namespace OTUS_Multithreading;

public sealed class Program
{
    private const int _threadCount = 5;
    private const int _size = 10000000;
        
    static void Main()
    {
        var computator = new ComputationService();
        
        var arr = ReturnRandomArr(_size);

        Meter(() => computator.ReturnUsualCalculation(arr), "ПОСЛЕДОВАТЕЛЬНОЕ");
        Meter(() => computator.ReturnParallelCalculation(arr, _threadCount), "ПАРАЛЛЕЛЬНОЕ");
        Meter(() => computator.ReturnParallelLinqCalculation(arr), "LINQ");
    }

    private static int[] ReturnRandomArr(int size)
    {
        Random r = new();
        
        var arr = new int[size];
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = r.Next(3);
        }
        return arr;
    }

    private static void Meter(Func<int> func, string type)
    {
        var sw = new Stopwatch();
        sw.Start();
        func();
        sw.Stop();
        Console.WriteLine($"\n{type}");
        Console.WriteLine($"Время вычисления: {GetTimeFormat(sw.Elapsed)}");
    }

    private static string GetTimeFormat(TimeSpan time)
    {
        return string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
        time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
    }

}