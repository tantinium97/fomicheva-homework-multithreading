﻿namespace OTUS_Multithreading;

public sealed class ComputationService : IComputationService
{
    private static readonly object _lock = new();

    public int ReturnUsualCalculation(int[] arr)
    {   
       return IsNullOrNullLenght(arr) ? 0 : arr.Sum();
    }

    public int ReturnParallelCalculation(int[] arr, int threadCount)
    {
        if (IsNullOrNullLenght(arr)) return 0;

        int resWithoutRem = arr.Length / threadCount;
        int rem = GetRemainder(arr.Length, threadCount);

        var threads = new List<Thread>();
        int sum = 0;

        for (int i = 0; i < threadCount - rem; i++)
        {
            int start = i * resWithoutRem;
            int stop = (i + 1) * resWithoutRem;

            var t = new Thread(() => Sum(arr, start, stop, ref sum));
            t.Start();
            threads.Add(t);
        }

        if (rem is not 0)
        {
            int start = (threadCount - 1) * resWithoutRem;
            int stop = arr.Length;

            var t = new Thread(() => Sum(arr, start, stop, ref sum));
            t.Start();
            threads.Add(t);
        }

        foreach (var t in threads)
        { 
            t.Join(); 
        }

        return sum;
    }

    public int ReturnParallelLinqCalculation(int[] arr)
    {
        return IsNullOrNullLenght(arr) ? 0 : arr.AsParallel().Sum();
    }

    private static bool IsNullOrNullLenght(int[] arr)
    {
        return arr is null || arr.Length == 0;
    }

    private static int GetRemainder(int lenght, int threadCount)
    {
        return lenght % threadCount is 0 ? 0 : 1;
    }

    private static void Sum(int[] arr, int start, int stop, ref int fullSum)
    {
        lock (_lock)
        {
            int sum = 0;

            for (int i = start; i < stop; i++)
            {
                sum += arr[i];
            }

            fullSum += sum;
        }
    }
}
