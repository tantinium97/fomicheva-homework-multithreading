﻿namespace OTUS_Multithreading;

public interface IComputationService
{
    int ReturnUsualCalculation(int[] arr);
    int ReturnParallelCalculation(int[] arr, int threadCount);
    int ReturnParallelLinqCalculation(int[] arr);
}
